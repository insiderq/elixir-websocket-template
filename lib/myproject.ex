defmodule MyProject do
	use Application
	def start do
		start(:normal, [])
	end
	def start(_type, _args) do
		dispatch = :cowboy_router.compile([{:_,
            [
            {"/ws/[...]",      :bullet_handler, [{:handler, Bullet}]},
            ] }])

        {:ok, _} = :cowboy.start_http(:http, 100, [port: 8000], 
            [env: [dispatch: dispatch]])

		MyProject.Supervisor.start_link
	end
	
end

defmodule MyProject.Supervisor do
	use Supervisor

	def start_link do
		Supervisor.start_link(__MODULE__, :ok)
	end

	def init(:ok) do
		children = [
			worker(Repo, []),
		]
		supervise(children, strategy: :one_for_one)
	end
end

